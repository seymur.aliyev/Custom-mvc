<?php

	return [
		"task_add_err_empty_username" => 'Имя не должно быть пустым',
        "task_add_err_empty_text" => 'Текст не должен быть пустым',
		'task_add_err_empty_email' => 'Email не должен быть пустым',
		'task_add_err_email_invalid' => 'Email неверный',
		'task_added_successfully' => 'Задача успешно добавлена',
		'task_checking_failed' => 'Ошибка при проверке задачи',
		'task_successfully_updated' => 'Задача успешно обновлена',
		'task_edit_text_is_empty' => 'Текст не должен быть пустым',
		'task_checked_successfully' => 'Текст изменён на "выполнено" успешно',
		'task_unchecked_successfully' => 'Текст изменён на "не выполнено" успешно',
		'auth_error_password_empty' => 'Пароль не должен быть пустым',
		'auth_error_username_empty' => 'Имя не должно быть пустым',
		'auth_error_username_not_found' => 'Пользователь не найден',
		'auth_error_password_invalid' => 'Пароль неверный',
	];
