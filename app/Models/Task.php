<?php
	
	namespace Models;
	
	use Core\Model;
	use PDO;
	
	class Task extends Model
	{
		public function all() {
			return $this->paginate('tasks');
		}
		
		public function add($data) {
			$this->db->insert('tasks', $data);
			return $this->db->lastInsertId();
		}
		
		public function show($id) {
			if (empty($id)) { return false;}
			
			$stmt = $this->db->prepare('SELECT * FROM tasks WHERE id=:id');
			$stmt->execute(['id' => $id]);
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
		
		public function modify($data) {
			return $this->db->prepare("UPDATE tasks SET text=:text, modified=1 WHERE id=:id")->execute($data);
		}
		
		public function check($data) {
			return $this->db->prepare("UPDATE tasks SET checked=:checked WHERE id=:id")->execute($data);
		}
	}
