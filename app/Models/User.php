<?php
	
	namespace Models;
	
	use Core\Model;
	use PDO;
	
	class User extends Model
	{
		public function getByLogin($username) {
			if (empty($username)) {return false;}
			
			$stmt = $this->db->prepare('SELECT login, password FROM users WHERE login=:username');
			$stmt->execute(['username' => $username]);
			return $stmt->fetch(PDO::FETCH_ASSOC);
		}
		
		public function passwordValid($password, $hashedPassword) {
			if (empty($password)) {return false;}
			
			return password_verify($password, $hashedPassword);
		}
	}
