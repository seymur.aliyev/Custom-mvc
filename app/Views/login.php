<div class="d-flex justify-content-center">
    <div class="col-md-6">
        <?php
            if (!empty($params['response']['errors'])) {
				foreach ($params['response']['errors'] as $error) { ?>
                    <div class="alert alert-danger" role="alert">
						<?=$error;?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
				<?php }
			}
        ?>
        <form method="POST" action="">
            <div class="form-group">
                <label for="username">Имя пользователя</label>
                <input type="text" name="username" class="form-control" id="username">
            </div>
            <div class="form-group">
                <label for="password">Пароль</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Войти</button>
            </div>
        </form>
    </div>
</div>
