<?php
    use Helpers\Utils;
?>
<html>
    <head>
        <title>MVC</title>
        <link rel="stylesheet" href="<?=BASE_URL.PUBLIC_DIR.'css/bootstrap.min.css';?>">
        <link rel="stylesheet" href="<?=BASE_URL.PUBLIC_DIR.'css/style.css';?>">
        <link rel="stylesheet" href="<?=BASE_URL.PUBLIC_DIR.'css/font-awesome/css/font-awesome.css';?>">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="<?=Utils::url('')?>">Custom MVC</a>
                <?php if (empty($_SESSION['user'])) { ?>
                    <a class="nav-link" href="<?=Utils::url('auth/login')?>">Войти</a>
                <?php } else { ?>
                    <a class="nav-link" href="<?=Utils::url('auth/logout')?>">Выйти</a>
                <?php } ?>
            </div>
        </nav>
        <div class="container pt-5">
        
