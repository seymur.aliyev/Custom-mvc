<?php
	
	namespace Core;
	
	class Bootstrap
	{
		private $url = null;
		private $controller = null;
		private $method = null;
		
		public function init()
		{
			$this->getUrl();
			
			if ($this->url=='/') {
				$this->loadDefaultController();
			}
			else {
				$this->loadController();
				$this->controllerMethod();
			}
		}
		
		private function getUrl()
		{
			$this->url = isset($_SERVER['PATH_INFO'])? explode('/', ltrim($_SERVER['PATH_INFO'], '/')) : '/';
		}
		
		private function loadController()
		{
			$className = ucfirst($this->url[0].'Controller');
			
			$file = $_SERVER['DOCUMENT_ROOT'].'/app/Controllers/'. $className . '.php';
			if (file_exists($file)) {
				$dirname = "\Controllers\\";
				$class = $dirname.$className;
				$this->controller = new $class();
			}
			else {
				$this->pageNotFound();
			}
		}
		
		private function controllerMethod()
		{
			if (isset($this->url[1]))
			{
				$this->method = $this->url[1];
				
				if (method_exists($this->controller, $this->method)) {
					$this->callMethod();
				}
				else {
					$this->pageNotFound();
				}
			}
			else {
				$this->pageNotFound();
			}
		}
		
		private function loadDefaultController()
		{
			$class = "\Controllers\IndexController";
			$this->controller = new $class();
			$this->method = "index";
			$this->callMethod();
		}
		
		private function pageNotFound()
		{
			$class = "\Core\Controller";
			$this->controller = new $class();
			$this->method = "action_404";
			$this->callMethod();
		}
		
		public function callMethod()
		{
			$this->controller->{$this->method}();
		}
	}
