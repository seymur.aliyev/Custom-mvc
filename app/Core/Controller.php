<?php
	
	namespace Core;
	
	class Controller
	{
		public function render($page, $params = []) {
			
			require_once  __DIR__.'../../Views/layouts/header.php';
			require_once  __DIR__.'../../Views/' . $page . '.php';
			require_once  __DIR__.'../../Views/layouts/footer.php';
			
			if (!empty($params) && is_array($params)) {
				extract($params);
			}
			
			ob_start();
			return ob_get_clean();
		}
		
		public function action_404() {
			return $this->render('404');
		}
		
		public function isAjax() {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
				return true;
			}
			return false;
		}
	}
