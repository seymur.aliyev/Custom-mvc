<?php
	
	namespace Controllers;
	
	use Core\Controller;
	use Helpers\Utils;
	use Models\Task;
	
	class TaskController extends Controller
	{
		public function edit() {
			
			if (empty($_SESSION)) {header('Location: '.BASE_URL);}
			if (!isset($_GET['id'])) {header('Location: '.BASE_URL);}
			
			$id = (int)$_GET['id'];
			if (empty($id)) {header('Location: '.BASE_URL);}
			
			$model = new Task();
			$task = $model->show($id);
			
			if (empty($task)) {header('Location: '.BASE_URL);}
			$params['task'] = $task;
			
			if (!empty($_POST)) {
				
				$text = $_POST['text'];
				
				if (empty($text)) {
					$params['response']['errors'][] = Utils::t('task_edit_text_is_empty');
				}
				
				if (empty($params['response']['errors'])) {
					if ($text != $task['text'] && !empty($_SESSION['user'])) {
						$data = ['text' => $text, 'id' => $task['id']];
						
						if ($model->modify($data)) {
							$params['response'] = [
								'success' => true,
								'message' => Utils::t('task_successfully_updated')
							];
						}
					}
				}
			}
			
			return $this->render('edit', $params);
		}
		
		public function check() {
			$response = [
				'success' => false,
				'message' => Utils::t('task_checking_failed')
			];
			
			if ($this->isAjax()) {
				if (empty($_SESSION)) {echo json_encode($response);exit();}
				
				$id = (int)$_GET['id'];
				if (empty($id)) {echo json_encode($response);exit();}
				
				$model = new Task();
				$task = $model->show($id);
				
				if (empty($task)) {echo json_encode($response);exit();}
				
				$value = $task['checked']? 0 : 1;
				$data = ['checked' => $value, 'id' => $task['id']];
				
				if ($model->check($data)) {
					$response = [
						'success' => true,
						'message' => Utils::t('task'. ($value? '_checked' : '_unchecked'). '_successfully')
					];
					
					echo json_encode($response);
					exit();
				}
			}
		}
	}
