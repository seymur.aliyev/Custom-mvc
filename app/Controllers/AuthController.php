<?php
	
	namespace Controllers;
	
	use Core\Controller;
	use Helpers\Utils;
	use Models\User;
	
	class AuthController extends Controller
	{
		public function login() {
			
			$params = [];
			
			if (!empty($_POST)) {
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				if (empty($password)) {
					$params['response']['errors'][] = Utils::t('auth_error_password_empty');
				}
				if (empty($username)) {
					$params['response']['errors'][] = Utils::t('auth_error_username_empty');
				}
				
				if (empty($params['response']['errors'])) {
					$model = new User();
					$user = $model->getByLogin($username);
					
					if (empty($user)) {
						$params['response']['errors'][] = Utils::t('auth_error_username_not_found');
					
					} else {
						
						if (!$model->passwordValid($password, $user['password'])) {
							$params['response']['errors'][] = Utils::t('auth_error_password_invalid');
						}
					}
					
					if (empty($params['response']['errors'])) {
						$_SESSION['user'] = $user;
						header('Location: '. Utils::url());
					}
				}
			}
			return $this->render('login', $params);
		}
		
		public function index() {
			return $this->render('index');
		}
		
		public function logout() {
			unset($_SESSION["user"]);
			header('Location: '. Utils::url());
		}
	}
