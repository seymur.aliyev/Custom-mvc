-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2021 at 02:21 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `custom_mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `modified` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `username`, `email`, `text`, `checked`, `modified`) VALUES
(1, 'Seymur', 'simone@zaza.ru', 'sdfsadfafasfs fsdfbasdjkfasd fdsjfasdkjfasdjkfdsf sdfjkdsfasdf', 1, 0),
(9, 'rufa', 'refa@az.az', 'sdfdssdfafdsfdsf', 1, 0),
(10, 'rufa', 'refa@az.az', 'sdfdssdfafdsfdsf', 0, 0),
(11, 'rufa', 'refa@az.az', 'sdfdssdfafdsfdsf', 0, 0),
(12, 'rufa', 'refa@az.az', 'sdfdssdfafdsfdsf', 0, 0),
(13, 'script', 'script@az.az', '<script>alert(\'mans\');</script>', 0, 0),
(14, 'mans', 'dans@mail.ru', '<script>alert(\'mans\');</script>', 1, 0),
(15, 'mans', 'dans@mail.ru', '<script>alert(\'mans\');</script>', 1, 0),
(16, 'sddf', 'asdfsd@ad.asd', 'sgsdgf', 0, 0),
(17, 'sddfsd', 'fds@sdaf.adsfa', 'dfdfdsfasd', 0, 0),
(18, 'fsdfs', 'asdfasdf@sdfl.sdf', 'dfasasdf', 0, 0),
(19, 'asdfadsf', 'asdfsdafa@asdf.dsaf', 'asdfsdaf', 0, 0),
(20, 'sadfasdf', 'asdfasf@asdf.sadfd', 'sadfdsf', 0, 0),
(21, 'dsfsadf', 'asdfsdaf@sdf.asdf', 'sdafasfda', 0, 0),
(22, 'dsfsadfasdf', 'sdfsadfas@asdfaa.asdf', 'dsfasd', 0, 0),
(23, 'sadfdsaf', 'sadfadsf@asdf.asfd', 'sdafdsafa', 0, 0),
(24, 'sdfdsf', 'sfsdfds@sdf.sdf', 'sdfsfgsd', 0, 0),
(25, 'dsfgsd', 'dsfgsdfg@sdf.asdf', 'dfgsfg', 0, 0),
(26, 'dsfgfdg', 'sdgdf@sffds.asdfsdf', 'fdgsdfgdd', 0, 0),
(27, 'zczcx', 'zxczx@sdffs.sdf', 'sdfdsf', 0, 0),
(28, 'dfgfsgf', 'dfsgfdgfdg.fdsg@sdf.dsf', 'dfgd', 0, 0),
(29, 'dfgsfg', 'dfgdfgs@dsfs.sdaf', 'dfgdfgsdf', 0, 0),
(30, 'sfdf', 'sdfdsf@sdf.dsf', 'dsfsdf', 0, 0),
(31, 'sdfdsf', 'sdfs@sdf.dsf', 'sdfdsf', 0, 0),
(32, 'sdfsdf', 'fdsfdsfs@sdfsdf.sdf', 'dsfdf', 0, 0),
(33, 'asdasdasd', 'asdd.asd@asd.asd', 'asdff', 0, 0),
(34, 'asdsa', 'asdas@da.asd', 'Ñ€ÑŒÑŒÑŒ', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'admin', '$2y$10$NCOW4C8Nt2lOMJndb16KoulqfnLfUV5AS4IzxJlzlg5uXOGaenCzK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
